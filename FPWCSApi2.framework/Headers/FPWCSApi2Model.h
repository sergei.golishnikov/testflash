//
//  FPWCSApi2Model.h
//  FPWCSApi
//
//  Created by user on 17/10/2016.
//  Copyright © 2016 user. All rights reserved.
//

#import "RTCEAGLVideoView.h"
#import "RTCConfiguration.h"
#import <JSONModel/JSONModel.h>

/**
 `kFPWCSConnectionStatus` is an enum reflecting status of connection to WCS server.
 */
typedef NS_ENUM(NSInteger, kFPWCSSessionStatus) {
    kFPWCSSessionStatusNew,
    kFPWCSSessionStatusPending,
    kFPWCSSessionStatusEstablished,
    kFPWCSSessionStatusDisconnected,
    kFPWCSSessionStatusFailed,
    kFPWCSSessionStatusRegistered,
    kFPWCSSessionStatusUnregistered
};

/**
 `kFPWCSConnectionStatus` is an enum reflecting status of connection to WCS server.
 */
typedef NS_ENUM(NSInteger, kFPWCSSessionEvent) {
    kFPWCSSessionEventData
};

/**
 `kFPWCSStreamStatus` is an enum reflecting stream status.
 */
typedef NS_ENUM(NSInteger, kFPWCSStreamStatus) {
    kFPWCSStreamStatusNew,
    kFPWCSStreamStatusPending,
    kFPWCSStreamStatusPublishing,
    kFPWCSStreamStatusPlaying,
    kFPWCSStreamStatusPaused,
    kFPWCSStreamStatusUnpublished,
    kFPWCSStreamStatusStopped,
    kFPWCSStreamStatusFailed,
    kFPWCSStreamStatusResize,
    kFPWCSStreamStatusSnaphotComplete,
    kFPWCSStreamStatusNotEnoughtBandwidth
};

/**
 `kFPWCSStreamStatus` is an enum reflecting stream status.
 */
typedef NS_ENUM(NSInteger, kFPWCSCallStatus) {
    kFPWCSCallStatusNew,
    kFPWCSCallStatusTrying,
    kFPWCSCallStatusRing,
    kFPWCSCallStatusBusy,
    kFPWCSCallStatusHold,
    kFPWCSCallStatusEstablished,
    kFPWCSCallStatusFailed,
    kFPWCSCallStatusFinish
};

/**
 `kFPWCSStreamStatusInfo` is an enum stream error.
 */
typedef NS_ENUM(NSInteger, kFPWCSStreamStatusInfo) {
    /**
     * Indicates general error during ICE negotiation. Usually occurs if client is behind some exotic nat/firewall.
     */
    kFPWCSStreamStatusInfoFailedByIceError,
    
    /**
     * Timeout has been reached during ICE establishment.
     */
    kFPWCSStreamStatusInfoFailedByIceTimeout,
    
    /**
     * ICE refresh failed on session.
     */
    kFPWCSStreamStatusInfoFailedByKeepAlive,
    
    /**
     * DTLS has wrong fingerprint.
     */
    kFPWCSStreamStatusInfoFailedByDtlsFingerprintError,
    
    /**
     * Client did not send DTLS packets or packets were lost/corrupted during transmission.
     */
    kFPWCSStreamStatusInfoFailedByDtlsError,
    
    /**
     * Indicates general HLS packetizer error, can occur during initialization or packetization (wrong input or out of disk space).
     */
    kFPWCSStreamStatusInfoFailedByHlsWriterError,
    
    /**
     * Indicates general RTMP republishing error, can occur during initialization or rtmp packetization.
     */
    kFPWCSStreamStatusInfoFailedByRtmpWriterError,
    
    /**
     * RTP session failed by RTP activity timer.
     */
    kFPWCSStreamStatusInfoFailedByRtpActivity,
    
    /**
     * Related session was disconnected.
     */
    kFPWCSStreamStatusInfoStoppedBySessionDisconnect,
    
    /**
     * Stream was stopped by rest terminate request.
     */
    kFPWCSStreamStatusInfoStoppedByRestTerminate,
    
    /**
     * Related publisher stopped its stream or lost connection.
     */
    kFPWCSStreamStatusInfoStoppedByPublisherStop,
    
    /**
     * Stop the media session by user after call was finished or unpublish stream.
     */
    kFPWCSStreamStatusInfoStoppedByUser,
    
    /**
     * Error occurred on the stream.
     */
    kFPWCSStreamStatusInfoFailedByError,
    
    /**
     * Indicates that error occurred during media session creation. This might be SDP parsing error, all ports are busy, wrong session related config etc.
     */
    kFPWCSStreamStatusInfoFailedToAddStreamToProxy,
    
    /**
     * Stopped shapshot distributor.
     */
    kFPWCSStreamStatusInfoDistributorStopped,
    
    /**
     * Publish stream is not ready, try again later.
     */
    kFPWCSStreamStatusInfoPublishStreamIsNotReady,
    
    /**
     * Stream with this name is not found, check the correct of the name.
     */
    kFPWCSStreamStatusInfoStreamNotFound,
    
    /**
     * Server already has a publish stream with the same name, try using different one.
     */
    kFPWCSStreamStatusInfoStreamNameAlreadyInUse,
    
    /**
     * Error indicates that stream object received by server has empty mediaSessionId field.
     */
    kFPWCSStreamStatusInfoMediasessionIdNull,
    
    /**
     * Published or subscribed sessions used this MediaSessionId.
     */
    kFPWCSStreamStatusInfoMediasessionIdAlreadyInUse,
    
    /**
     * Session is not initialized or terminated on play ordinary stream.
     */
    kFPWCSStreamStatusInfoSessionNotReady,
    
    /**
     * Actual session does not exist.
     */
    kFPWCSStreamStatusInfoSessionDoesNotExist,
    
    /**
     * RTSP has wrong format on play stream, check correct of the RTSP url.
     */
    kFPWCSStreamStatusInfoRtspHasWrongFormat,
    
    /**
     * Failed to play vod stream, this format is not supported.
     */
    kFPWCSStreamStatusInfoFileHasWrongFormat,
    
    /**
     * Failed to connect to rtsp stream.
     */
    kFPWCSStreamStatusInfoFailedToConnectToRtspStream,
    
    /**
     * Rtsp stream is not found, agent received "404-Not Found".
     */
    kFPWCSStreamStatusInfoRtspStreamNotFound,
    
    /**
     * On shutdown RTSP agent.
     */
    kFPWCSStreamStatusInfoRtspAgentShutdown,
    
    /**
     * Stream failed
     */
    kFPWCSStreamStatusInfoStreamFailed,
    
    /**
     * No common codecs on setup track, did not found corresponding trackId->mediaPort.
     */
    kFPWCSStreamStatusInfoNoCommonCodecs,
    
    /**
     * Bad referenced rtsp link, check for correct, example: rtsp://user:b@d_password@127.0.0.1/stream.
     */
    kFPWCSStreamStatusInfoBadUri,
    
    /**
     * General VOD error, indicates that Exception occurred while reading/processing media file.
     */
    kFPWCSStreamStatusInfoGotExceptionWhileStreamingFile,
    
    /**
     * Requested stream shutdown.
     */
    kFPWCSStreamStatusInfoRequestedStreamShutdown,
    
    /**
     * Failed to create movie, file can not be read.
     */
    kFPWCSStreamStatusInfoFailedToReadFile,
    
    /**
     * File does not exist, check filename.
     */
    kFPWCSStreamStatusInfoFileNotFound,
    
    /**
     * Server failed to establish websocket connection with origin server.
     */
    kFPWCSStreamStatusInfoFailedToConnectToOriginStream,
    
    /**
     * CDN stream not found.
     */
    kFPWCSStreamStatusInfoCdnStreamNotFound,
    
    /**
     * Indicates that provided URL protocol in stream name is invalid.
     * Valid: vod://file.mp4
     * Invalid: dov://file.mp4
     */
    kFPWCSStreamStatusInfoFailedToGetAgentStorage,
    
    /**
     * Shutdown agent servicing origin stream.
     */
    kFPWCSStreamStatusInfoAgentServicingOriginStreamIsShuttingDown,
    
    /**
     * Terminated by keep-alive on walk through subscribers.
     */
    kFPWCSStreamStatusInfoTerminatedByKeepAlive,
	
    /**
     * Transcoding required, but disabled
     */
	kFPWCSStreamStatusInfoTranscodingRequiredButDisabled
};

/**
 `kFPWCSCallStatusInfo` is an enum call error.
 */
typedef NS_ENUM(NSInteger, kFPWCSCallStatusInfo) {
    /**
     * Normal call hangup.
     */
    kFPWCSCallStatusInfoNormalCallClearing,
    
    /**
     * Error occurred on session creation.
     */
    kFPWCSCallStatusInfoFailedBySessionCreation,
    
    /**
     * Failed by error during ICE establishment.
     */
    kFPWCSCallStatusInfoFailedByIceError,
    
    /**
     * RTP session failed by RTP activity timer.
     */
    kFPWCSCallStatusInfoFailedByRtpActivity,
    
    /**
     * FF writer was failed on RTMP.
     */
    kFPWCSCallStatusInfoFailedByRtmpWriterError,
    
    /**
     * DTLS wrong fingerprint.
     */
    kFPWCSCallStatusInfoFailedByDtlsFingerprintError,
    
    /**
     * Client did not send DTLS packets or packets were lost/corrupted during transmission.
     */
    kFPWCSCallStatusInfoFailedByDtlsError,
    
    /**
     * Error occurred during call.
     */
    kFPWCSCallStatusInfoFailedByError,
    
    /**
     * Call failed by request timeout.
     */
    kFPWCSCallStatusInfoFailedByRequestTimeout,
	
	/**
     * Transcoding required, but disabled
     */
	kFPWCSCallStatusInfoTranscodingRequiredButDisabled,
	
	/**
    * No common codecs
    */
	kFPWCSCallStatusInfoNoCommonCodecs
};

/**
 Static helpers
 **/
@interface FPWCSApi2Model : NSObject

/**
 Convert session status to string
 
 @param status `kFPWCSSessionStatus` session status
 
 @return `NSString` status string representation
 **/
+ (NSString *)sessionStatusToString:(kFPWCSSessionStatus)status;

/**
 Convert stream status to string
 
 @param status `kFPWCSStreamStatus` stream status
 
 @return `NSString` status string representation
 **/
+ (NSString *)streamStatusToString:(kFPWCSStreamStatus)status;

+ (NSString *)callStatusToString:(kFPWCSCallStatus)status;

+ (kFPWCSCallStatus)callStatusFromString:(NSString*)statusString;

/**
 Convert string to stream status
 
 @param statusString `NSString` stream status
 
 @return `kFPWCSStreamStatus` status enum representation
 **/
+ (kFPWCSStreamStatus)streamStatusFromString:(NSString*)statusString;

/**
 Convert stream info to string
 
 @param status `kFPWCSStreamStatusInfo` stream status
 
 @return `NSString` status info string representation
 **/
+ (NSString*)streamStatusInfoToString:(kFPWCSStreamStatusInfo)status;

/**
 Convert string to stream info
 
 @param statusString `NSString` stream info
 
 @return `kFPWCSStreamStatusInfo` status info enum representation
 **/
+ (kFPWCSStreamStatusInfo)streamStatusInfoFromString:(NSString*)statusString;

/**
 Convert call info to string
 
 @param status `kFPWCSCallStatusInfo` call status
 
 @return `NSString` status info string representation
 **/
+ (NSString*)callStatusInfoToString:(kFPWCSCallStatusInfo)status;

/**
 Convert string to call info
 
 @param statusString `NSString` call info
 
 @return `kFPWCSCallStatusInfo` status info enum representation
 **/
+ (kFPWCSCallStatusInfo)callStatusInfoFromString:(NSString*)statusString;

@end


/**
 Video contraints
 **/
@interface FPWCSApi2AudioConstraints : NSObject
@property BOOL useFEC;
@property BOOL useStereo;
@property NSInteger bitrate;
@end

/**
 Video contraints
 **/
@interface FPWCSApi2VideoConstraints : NSObject

@property NSString *deviceID;

@property NSInteger minWidth;

@property NSInteger minHeight;

@property NSInteger maxWidth;

@property NSInteger maxHeight;

@property NSInteger minFrameRate;

@property NSInteger maxFrameRate;

@property NSInteger bitrate;

@property NSInteger quality;

@end


/**
 Media constraints
 **/
@interface FPWCSApi2MediaConstraints : NSObject

@property FPWCSApi2AudioConstraints *audio;

@property FPWCSApi2VideoConstraints *video;

- (instancetype)initWithAudio:(BOOL)audio video:(BOOL)video;

- (instancetype)initWithAudio:(BOOL)audio videoWidth:(NSInteger)width videoHeight:(NSInteger)height;

- (instancetype)initWithAudio:(BOOL)audio videoWidth:(NSInteger)width videoHeight:(NSInteger)height videoFps:(NSInteger)fps;

@end

/**
 Session options
 **/
@interface FPWCSApi2SessionOptions : NSObject

@property NSString *urlServer;

@property NSString *appKey;

@property RTCConfiguration *mediaConfig;

@property BOOL sipRegisterRequired;

@property NSString *sipLogin;

@property NSString *sipAuthenticationName;

@property NSString *sipPassword;

@property NSString *sipVisibleName;

@property NSString *sipDomain;

@property NSString *sipOutboundProxy;

@property NSNumber *sipPort;

@property NSMutableDictionary *custom;

@property NSObject *sipOptions;

@end

/**
 Stream options
 **/
@interface FPWCSApi2StreamOptions : NSObject

@property NSString *name;

@property FPWCSApi2MediaConstraints *constraints;

@property NSString *rtmpUrl;

@property BOOL record;

@property RTCEAGLVideoView *display;

@property NSMutableDictionary *custom;

@end

/**
 Call options
 **/
@interface FPWCSApi2CallOptions : NSObject

@property NSString *callee;

@property NSMutableDictionary *inviteParameters;

@property FPWCSApi2MediaConstraints *localConstraints;

@property FPWCSApi2MediaConstraints *remoteConstraints;

@property RTCEAGLVideoView *localDisplay;

@property RTCEAGLVideoView *remoteDisplay;

@property NSMutableDictionary *custom;

@end

/**
 Media device info
 **/
@interface FPWCSApi2MediaDevice : NSObject

/**
 Can be one of AVMediaTypeVideo, AVMediaTypeAudio
 **/
@property NSString *type;

/**
 Device unique id
 **/
@property NSString *deviceID;

/**
 Device localized name
 **/
@property NSString *label;

@end

/**
 Media devices
 **/
@interface FPWCSApi2MediaDeviceList : NSObject

/**
 Array containing `FPWCSApi2MediaDevice` devices with type audio
 **/
@property NSMutableArray *audio;

/**
 Array containing `FPWCSApi2MediaDevice` devices with type video
 **/
@property NSMutableArray *video;

@end


@interface FPWCSApi2Data : JSONModel

@property NSString *operationId;
@property NSDictionary<Optional> *payload;
@property NSString<Optional> *status;
@property NSString<Optional> *info;

@end



