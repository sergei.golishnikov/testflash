//
//  ViewController.swift
//  TestW
//
//  Created by Sergei Golishnikov on 21/05/2020.
//  Copyright © 2020 Sergei Golishnikov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        connect()
    }

    
    func connect() {
        let options = FPWCSApi2RoomManagerOptions()
        options.urlServer = "https://flashphoner.cnnct.support:8444"
        options.username = "admin"
        
        do {
            let roomManager = try FPWCSApi2.createRoomManager(options)
            
            roomManager.on(.fpwcsRoomManagerEventConnected) { _ in
                debugPrint("=== connected")
            }
            
            roomManager.on(.fpwcsRoomManagerEventFailed) { _ in
                debugPrint("=== failed")
            }
            
            roomManager.on(.fpwcsRoomManagerEventDisconnected) { _ in
                debugPrint("--- disconnected")
            }
            
        } catch {
            debugPrint(error)
        }
        
    }

}

